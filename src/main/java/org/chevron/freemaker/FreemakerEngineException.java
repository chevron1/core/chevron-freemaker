package org.chevron.freemaker;

import org.chevron.model.TemplateEngineException;

public class FreemakerEngineException extends TemplateEngineException {

    public FreemakerEngineException(String message) {
        super(message);
    }

    public FreemakerEngineException(String message, Throwable cause) {
        super(message, cause);
    }

}
