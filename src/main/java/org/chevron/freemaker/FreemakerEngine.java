package org.chevron.freemaker;

import freemarker.template.Configuration;
import org.chevron.model.TemplateEngine;
import org.chevron.model.TemplatingContext;

import java.util.HashMap;
import java.util.Map;

public class FreemakerEngine implements TemplateEngine<Configuration> {

    private static final String LANGUAGE = "freemaker";

    public String getLanguage() {
        return LANGUAGE;
    }

    public TemplatingContext createContext(Configuration config, Map<String,Object> definitions) {
        if(config==null)
            throw new IllegalArgumentException("config must not be null nor empty");
        if(definitions==null)
            definitions = new HashMap<>();
        return new FreemakerContext(config,
                definitions);
    }
}
