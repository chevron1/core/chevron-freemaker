package org.chevron.freemaker;

import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.chevron.model.TemplatingContext;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class FreemakerContext implements TemplatingContext {

    private static ThreadLocal<Supplier<String>> TEMPLATE_SUPPLIER = new ThreadLocal<Supplier<String>>();
    private Configuration configuration;
    private Map<String,Object> definitions = new HashMap<>();

    FreemakerContext(Configuration config, Map<String,Object> map){
        this.configuration = config;
        this.definitions.putAll(map);
        config.setTemplateLoader(new TemplateLoader() {

            public Object findTemplateSource(String name) throws IOException {
                return TEMPLATE_SUPPLIER.get();
            }

            public long getLastModified(Object o) {
                return -1;
            }

            public Reader getReader(Object object, String s) throws IOException {
                Supplier<String> supplier = (Supplier<String>) object;
                return new StringReader(supplier.
                        get());
            }

            public void closeTemplateSource(Object o) throws IOException {

            }

        });
    }

    @Override
    public String merge(Supplier<String> sourceSupplier, Map<String, Object> properties) {
        if(sourceSupplier==null)
            throw new IllegalArgumentException("sourceSupplier must not be null");
        if(properties==null)
            properties = new HashMap<>();
        TEMPLATE_SUPPLIER.set(sourceSupplier);
        try {
            Map<String,Object> model = new HashMap<>();
            model.putAll(definitions);
            model.putAll(properties);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            PrintWriter printWriter = new PrintWriter(bout);
            configuration.getTemplate("this").process(model, printWriter);
            return new String(bout.toByteArray(), "utf-8");
        }catch (IOException | TemplateException ex) {
            throw new FreemakerEngineException("error processing template",
                    ex);
        }finally {
            TEMPLATE_SUPPLIER.remove();
        }
    }

    public String merge(String source, Map<String,Object> map) {
        if(source==null||source.isEmpty())
            throw new IllegalArgumentException("source must not be null nor empty");
        return this.merge(() -> source,
                map);
    }

    @Override
    public void next() {

    }

}
